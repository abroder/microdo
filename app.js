class TodoList extends React.Component {
  constructor(props) {
    super(props);
  }

  createItem(item) {
    return <li>{item}</li>;
  }

  render() {
    return <ul>{ this.props.items.map(this.createItem)}</ul>;
  }
}

class TodoApp extends React.Component {
  constructor(props) {
    this.state = {items: ["Hello", "Goodbye"]};
  }

  render() {
    return (
      <div id='todo-app'>
        <TodoList items={this.state.items} />
      </div>
    );
  }
}

React.render(<TodoApp />, document.getElementById('example'))
